<?php

namespace Drupal\Tests\node_boolean\Unit\Plugin\Condition;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Tests\UnitTestCase;
use Drupal\node\Entity\Node;
use Drupal\Core\Plugin\Context\Context;
use Drupal\node_boolean\Plugin\Condition\NodeBoolean;
use Drupal\Core\Field\Plugin\Field\FieldType\BooleanItem;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Tests a node boolean condition.
 *
 * @coversDefaultClass \Drupal\node_boolean\Plugin\Condition\NodeBoolean
 *
 * @group node_boolean
 */
class NodeBooleanConditionTest extends UnitTestCase {

  /**
   * Test for the presence of ANY boolean fields.
   *
   * | field_test_one = true  |
   * | should return true     |
   */
  public function testAnyConditionWithSingleTrueValueEvaluatesToTrue() {
    $context = $this->mockNodeContextWithValues([
      'field_test_one' => TRUE,
    ]);

    $configuration = [
      'all' => FALSE,
      'boolean' => [
        'field_test_one' => 'field_test_one',
      ],
    ];
    $condition = $this->mockNodeBoolean($configuration);

    $condition->setContext('node', $context);

    $this->assertTrue($condition->evaluate());
  }

  /**
   * Test ANY boolean fields are true.
   *
   * | field_test_one = true  |
   * | field_test_two = false |
   * | should return true     |
   */
  public function testMultipleAnyConditionWithSingleTrueValueEvaluatesToTrue() {
    $context = $this->mockNodeContextWithValues([
      'field_test_one' => TRUE,
      'field_test_two' => FALSE,
    ]);

    $configuration = [
      'all' => FALSE,
      'boolean' => [
        'field_test_one' => 'field_test_one',
        'field_test_two' => 'field_test_two',
      ],
    ];
    $condition = $this->mockNodeBoolean($configuration);

    $condition->setContext('node', $context);

    $this->assertTrue($condition->evaluate());
  }

  /**
   * Test ANY boolean fields are true, when all are false.
   *
   * | field_test_one = false  |
   * | field_test_two = false  |
   * | should return false     |
   */
  public function testMultipleAnyConditionWithAllFalseValuesEvaluatesToFalse() {
    $context = $this->mockNodeContextWithValues([
      'field_test_one' => FALSE,
      'field_test_two' => FALSE,
    ]);

    $configuration = [
      'all' => FALSE,
      'boolean' => [
        'field_test_one' => 'field_test_one',
        'field_test_two' => 'field_test_two',
      ],
    ];
    $condition = $this->mockNodeBoolean($configuration);

    $condition->setContext('node', $context);

    $this->assertFalse($condition->evaluate());
  }

  /**
   * Test ALL Boolean fields are true evaluates to true.
   *
   * | field_test_one = true  |
   * | field_test_two = true  |
   * | should return true     |
   */
  public function testMultipleAllConditionWithAllTrueValuesEvaluatesToTrue() {
    $context = $this->mockNodeContextWithValues([
      'field_test_one' => TRUE,
      'field_test_two' => TRUE,
    ]);

    $configuration = [
      'all' => TRUE,
      'boolean' => [
        'field_test_one' => 'field_test_one',
        'field_test_two' => 'field_test_two',
      ],
    ];
    $condition = $this->mockNodeBoolean($configuration);

    $condition->setContext('node', $context);

    $this->assertTrue($condition->evaluate());
  }

  /**
   * Test ALL Boolean fields are a mix of true and false evaluates to false.
   *
   * | field_test_one = true  |
   * | field_test_two = false |
   * | should return false    |
   */
  public function testMultipleAllConditionWithTrueAndFalseValuesEvaluatesToFalse() {
    $context = $this->mockNodeContextWithValues([
      'field_test_one' => TRUE,
      'field_test_two' => FALSE,
    ]);

    $configuration = [
      'all' => TRUE,
      'boolean' => [
        'field_test_one' => 'field_test_one',
        'field_test_two' => 'field_test_two',
      ],
    ];
    $condition = $this->mockNodeBoolean($configuration);

    $condition->setContext('node', $context);

    $this->assertFalse($condition->evaluate());
  }

  /**
   * Test ALL Boolean fields are a mix of true and false evaluates to false.
   *
   * | field_test_one = false |
   * | field_test_two = true  |
   * | should return false    |
   */
  public function testMultipleAllConditionWithFalseAndTrueValuesEvaluatesToFalse() {
    $context = $this->mockNodeContextWithValues([
      'field_test_one' => FALSE,
      'field_test_two' => TRUE,
    ]);

    $configuration = [
      'all' => TRUE,
      'boolean' => [
        'field_test_one' => 'field_test_one',
        'field_test_two' => 'field_test_two',
      ],
    ];
    $condition = $this->mockNodeBoolean($configuration);

    $condition->setContext('node', $context);

    $this->assertFalse($condition->evaluate());
  }

  /**
   * Test ALL Boolean fields are a mix of true and false evaluates to false.
   *
   * | field_test_one = false  |
   * | field_test_two = true   |
   * | field_test_three = true |
   * | should return false     |
   */
  public function testMultipleAllConditionWithFalseAndTrueAndTrueValuesEvaluatesToFalse() {
    $context = $this->mockNodeContextWithValues([
      'field_test_one' => FALSE,
      'field_test_two' => TRUE,
      'field_test_three' => TRUE,
    ]);

    $configuration = [
      'all' => TRUE,
      'boolean' => [
        'field_test_one' => 'field_test_one',
        'field_test_two' => 'field_test_two',
        'field_test_three' => 'field_test_three',
      ],
    ];
    $condition = $this->mockNodeBoolean($configuration);

    $condition->setContext('node', $context);

    $this->assertFalse($condition->evaluate());
  }

  /**
   * Test ALL Boolean fields are a mix of true and false evaluates to false.
   *
   * | [ ] field_test_one = false  |
   * | [*] field_test_two = true   |
   * | [*] field_test_three = true |
   * |     should return true      |
   */
  public function testMultipleAllConditionWithFalseAndTrueAndTrueValuesEvaluatesToTrueAsFalseFieldIsUnused() {
    $context = $this->mockNodeContextWithValues([
      'field_test_one' => FALSE,
      'field_test_two' => TRUE,
      'field_test_three' => TRUE,
    ]);

    $configuration = [
      'all' => TRUE,
      'boolean' => [
        'field_test_two' => 'field_test_two',
        'field_test_three' => 'field_test_three',
      ],
    ];
    $condition = $this->mockNodeBoolean($configuration);

    $condition->setContext('node', $context);

    $this->assertTrue($condition->evaluate());
  }

  /**
   * Test check for a field that doesn't exist, returns FALSE.
   */
  public function testSingleAnyConditionWithoutFieldEvaluatesToFalse() {
    $nodeFields = [];
    $context = $this->mockNodeContextWithValues($nodeFields);

    $configuration = [
      'all' => FALSE,
      'boolean' => [
        'field_test_two' => 'field_test_two',
      ],
    ];
    $condition = $this->mockNodeBoolean($configuration, $nodeFields);

    $condition->setContext('node', $context);

    $this->assertFalse($condition->evaluate());
  }

  /**
   * Get a node boolean condition plugin with a mocked entity field manager.
   */
  public function mockNodeBoolean($configuration, $nodeFields = ['field_test_one', 'field_test_two', 'field_test_three']) {
    $fields['node'] = [];
    foreach ($nodeFields as $field) {
      $fields['node'][$field] = [
          'bundles' => [
            'test_type' => TRUE,
          ],
      ];
    }

    $entityFieldManager = $this->getMockBuilder(EntityFieldManager::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entityFieldManager->expects($this->any())
      ->method('getFieldMapByFieldType')
      ->will($this->returnValue($fields));

    $plugin_id = 'node_boolean';

    $plugin_definition = ['provider' => 'unit_test'];

    return new NodeBoolean($configuration, $plugin_id, $plugin_definition, $entityFieldManager);
  }

  /**
   * Generate a mocked node with the field values defined in the values array.
   *
   * @param array $values
   *   The field values to be assigned to the mocked node.
   */
  protected function mockNodeContextWithValues(array $values) {
    $node = $this->getMockBuilder(Node::class)
      ->disableOriginalConstructor()
      ->getMock();
    $node->expects($this->any())
      ->method('getType')
      ->will($this->returnValue('test_type'));

    $field_values = [];
    foreach ($values as $field_key => $value) {
      $field_values[] = [$field_key, $this->mockBooleanFieldValue($value)];
    }

    $node->expects($this->any())
      ->method('get')
      ->willReturnMap($field_values);

    $context = $this->getMockBuilder(Context::class)
      ->disableOriginalConstructor()
      ->getMock();
    $context->expects($this->any())
      ->method('getcontextvalue')
      ->will($this->returnValue($node));

    return $context;
  }

  /**
   * Mock the field value.
   */
  protected function mockBooleanFieldValue($value) {
    $booleanMock = $this->getMockBuilder(BooleanItem::class)
      ->disableOriginalConstructor()
      ->getMock();
    $booleanMock->expects($this->any())
      ->method('getValue')
      ->willReturn($value);

    $fieldValueMock = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $fieldValueMock->expects($this->any())
      ->method('get')
      ->with('value')
      ->willReturn($booleanMock);

    $fieldListValueMock = $this->getMockBuilder(FieldItemListInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $fieldListValueMock->expects($this->any())
      ->method('first')
      ->willReturn($fieldValueMock);
    $fieldListValueMock->expects($this->any())
      ->method('count')
      ->willReturn(1);

    return $fieldListValueMock;
  }

}
